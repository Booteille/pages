# Pages
This repository contains my Gitea pages.
These are small hobbies projects which can be useful for some.
I don't expect to give a lot of time to maintain these.
All my work is under FLOSS licence so feel free to fork it.


## [Invimatrix](https://booteille.codeberg.page/invimatrix/)
Invimatrix generates rules to allow #uMatrix and uBlock Origin to access the content of Piped and Invidious known instances.


## [PeerTubatrix](https://booteille.codeberg.page/peertubatrix/)
PeerTubatrix generates rules to allow #uMatrix and uBlock Origin to access the content of PeerTube instances available on https://instances.joinpeertube.org.


## [LemmyMatrix](https://booteille.codeberg.page/lemmymatrix/)
LemmyMatrix generates rules to allow #uMatrix and uBlock Origin to access the content of known Lemmy instances.

----

## [PeerTube Extension Finder](https://booteille.codeberg.page/peertube-extensions-finder/)
PeerTube Extension Finder allows you to search over available PeerTube extensions.
