const registryURL = 'https://api.npms.io/v2';
let themes = {
    results: null
};

let plugins = {
    results: null
};


const registryError = (msg) => {
    console.error(`An error occurred while trying to call the registry: ${msg}.`);
}

const registryWarning = (msg) => {
    console.warn(`An error occurred while trying to call the registry: ${msg}. The parameter has been removed from the API call.`);
}

/**
 * Retrieve all plugins available from npmjs registry
 */
const getPlugins = async () => {
    const nb = 250;
    const p = await searchFromRegistry('peertube-plugin-', nb);
    plugins.results = p.results;

    if (p.total > p.results.length) {
        const n = Math.round(p.total / nb);


        let i = 1;
        for (i; i < n; i++) {
        console.debug(n)
            plugins.results = plugins.results.concat((await searchFromRegistry('peertube-plugin-', nb, nb * i)).results);
        }
    }

    console.log(`${plugins.results.length} plugins retrieved from npmjs registry!`);
}

/**
 * Retrieve all themes available from npmjs registry
 */
const getThemes = async () => {
    const nb = 250;
    const t = await searchFromRegistry('peertube-theme-', nb);
    themes.results = t.results;

    if (t.total > t.results.length) {
        const n = Math.round(t.total / nb);

        let i = 1;
        for (i; i < n; i++) {
            themes.results = themes.results.concat((await searchFromRegistry('peertube-theme-', nb, nb * i)).results);
        }
    }

    console.log(`${themes.results.length} themes retrieved from npmjs registry!`);
}

const searchFromRegistry = async (search = null, size = null, from = null, quality = null, popularity = null, maintenance = null) => {
    let searchEndpoint = `${registryURL}/search?`;

    if (search !== null) {
        if (typeof search === 'string') {
            searchEndpoint += `q=${encodeURIComponent(search)}&`;
        } else {
            registryWarning('text parameter must be a string');
        }
    }

    if (size !== null) {
        if (typeof size === 'number' && Number.isSafeInteger(size)) {
            searchEndpoint += `size=${size}&`;
        } else {
            registryWarning('size parameter must be an integer');
        }
    }

    if (from !== null) {
        if (typeof from === 'number' && Number.isSafeInteger(from)) {
            searchEndpoint += `from=${from}&`;
        } else {
            registryWarning('from parameter must be an integer');
        }
    }

    if (quality !== null) {
        if (typeof quality === 'number') {
            searchEndpoint += `quality=${quality}&`;
        } else {
            registryWarning('quality parameter must be a float');
        }
    }

    if (popularity !== null) {
        if (typeof popularity === 'number') {
            searchEndpoint += `popularity=${popularity}&`;
        } else {
            registryWarning('popularity parameter must be a float');
        }
    }

    if (maintenance !== null) {
        if (typeof maintenance === 'number') {
            searchEndpoint += `maintenance=${maintenance}&`;
        } else {
            registryWarning('maintenance parameter must be a float');
        }
    }

    searchEndpoint = searchEndpoint.substring(0, searchEndpoint.length - 1);

    const res = (await fetch(searchEndpoint)).json();
    const results = (await res).results.filter((o) => {
        return o.package.name.startsWith("peertube-plugin") || o.package.name.startsWith("peertube-theme");
    });

    return { results, total: (await res).total };
};

const handleSearch = (search, type, thoughTags = true) => {
    if (type !== "theme" && type !== "plugin") {
        registryError('You must provide a valid search type');
        return null;
    }

    if (typeof search === 'string') {
        let res = [];
        const tp = type == 'theme' ? themes : plugins;

        search = search.split(' ');

        let j = 0;
        for (j; j < tp.results.length; j++) {
            let termsFound = true; // All terms must be found in title
            let tagsFound = false; // Only one term must be found in tags

            let i = 0;
            for (i; i < search.length; i++) {
                // Search corresponding titles
                if (!tp.results[j].package.name.toLowerCase().includes(search[i].toLowerCase())) {
                    termsFound = false;
                }

                // Search corresponding tags
                if (thoughTags) {
                    if(tp.results[j].keywords) {
                        if (tp.results[j].package.keywords.find(e => {
                            return e.toLowerCase().includes(search[i].toLowerCase())
                        }) !== undefined) {
                            tagsFound = true;
                        }
                    }
                }
            }


            if (termsFound || tagsFound) {
                res.push(tp.results[j]);
            }
        }

        return res;
    } else {
        registryError('search parameter must be a string');
    }
}

/**
 * Search through the plugins list
 *
 * @param {string | null} search
 * @returns
 */
const searchPlugins = async (search = null) => {
    if (search !== null) {
        return handleSearch(search, 'plugin');
    }

    return null;
};

const searchThemes = async (search = null) => {
    if (search !== null) {
        return handleSearch(search, 'theme');
    }

    return null;
};
